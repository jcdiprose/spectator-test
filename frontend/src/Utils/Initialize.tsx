import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import { Store } from '../Redux/Store'
import { requestNews } from '../Redux/News/Actions'

interface InitializeProps {
  children: JSX.Element
}

const Title = styled.h1`
  font-size: 2em;
  text-align: center;
`

const Initialize = (props: InitializeProps) => {
  const dispatch = useDispatch()

  const is_loading = useSelector((store: Store) => store.news.loading)

  useEffect(() => {
    dispatch(requestNews())
  }, [])

  if (is_loading) {
    return <Title>LOADING</Title>
  }

  return props.children
}

export default Initialize
