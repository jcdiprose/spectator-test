import axios from 'axios'

export const getNews = async () => {
  const news = await axios.get('http://localhost:4000/coronavirus-news')
  return news
}
