import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

const el = document.createElement('link')
el.rel = 'stylesheet'
el.href = 'https://use.typekit.net/xkz5olp.css'
document.body.appendChild(el)

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)
