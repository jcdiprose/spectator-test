import styled from 'styled-components'
import { GRID_PADDING, SCROLL_ITEM_WIDTH } from '../Constants'

export const Author = styled.h2`
  padding-top: 6px;
  border-top: 1px none #000;
  border-bottom: 1px none #000;
  color: #d30d1d;
  font-size: 22px;
  line-height: 1.17;
  font-style: italic;
  letter-spacing: -0.05px;
  font-family: 'Goudy Old Style';
  margin-top: ${GRID_PADDING}px;
  max-width: 90%;
  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }
`
export const Headline = styled.h1`
  margin-bottom: 20px;
  color: #222;
  font-size: 28px;
  line-height: 1.17;
  font-style: normal;
  font-weight: 600;
  letter-spacing: -0.05rem;
  font-family: 'Goudy Old Style';
  overflow-y: ${(props) => (props.isBookmark ? 'hidden' : '')};
  width: 100%;
  max-height: ${(props) => (props.isBookmark ? '60px' : '')};
`
export const RegularGoudy = styled.h3`
  display: block;
  width: ${(props) => (props.setWidth ? props.setWidth : 'auto')};
  color: #d30d1d;
  font-size: 22px;
  font-weight: 200;
  line-height: 1.2;
  letter-spacing: -0.05px;
  font-family: 'Goudy Old Style';
  margin-top: ${GRID_PADDING}px;
`

export const SansDisplay = styled.p`
  display: block;
  font-size: 14px;
  line-height: 1.43;
  font-family: 'Roboto';
  font-weight: 600;
  color: rgba(38, 34, 34, 0.6);
  margin-top: ${GRID_PADDING}px;
`

export const Button = styled.button`
  display: flex;
  align-items: center;
  border: ${(props) => (props.is_bookmarked ? '1.5px solid rgba(38, 34, 34, 0.15)' : '1.5px solid #d30d1d')};
  border-radius: 100px;
  box-shadow: ${(props) =>
    props.is_bookmarked ? 'none' : '4px 4px 0 0 rgba(211, 13, 29, 0.05), inset 4px 4px 0 0 #ffffff'};
  background-color: ${(props) => (props.is_bookmarked ? 'rgba(38, 34, 34, 0.05)' : 'rgba(211, 13, 29, 0.05)')};
  color: ${(props) => (props.is_bookmarked ? 'rgba(38, 34, 34, 0.4)' : '#d30d1d')};
  font-size: 12px;
  font-family: 'josefin-sans';
  font-weight: 600;
  text-transform: uppercase;
  padding: 6px 20px;
  cursor: pointer;
  transition: all 0.5s ease;
  outline: none;

  span {
    position: relative;
    top: 2px;
  }

  &:hover {
    ${(props) =>
      !props.is_bookmarked &&
      `border-color: #f74b5d;
      color: #f74b5d;
      box-shadow: none;
      background-color: white;`}
  }
`
export const Image = styled.img`
  width: 100%;
  height: auto;
  margin-top: ${GRID_PADDING}px;
  margin-bottom: ${GRID_PADDING}px;
`

export const Scroller = styled.div`
  overflow-x: scroll;

  @media screen and (min-width: 992px) {
    overflow-x: unset;
  }
`

export const ScrollContainer = styled.div`
  width: ${(props) => props.scrollableWidth + 'px'};
  height: ${(props) => props.setHeight};
  display: flex;
  flex-direction: row;

  @media screen and (min-width: 992px) {
    width: 100%;
    display: block;

    &::before {
      margin-top: ${GRID_PADDING}px;
      display: block;
      content: '';
      width: 100%;
      background: #d30d1d;
      height: 1px;
      box-shadow: 0px 4px 0px 0px rgba(211, 13, 29, 0.2), 0px 8px 0px 0px rgba(211, 13, 29, 0.2);
    }
  }
`

export const ScrollItem = styled.div`
  position: relative;
  width: ${(props) => {
    if (props.setWidth) return props.setWidth
    return `${SCROLL_ITEM_WIDTH}px`
  }};
  height: auto;
  padding-right: ${GRID_PADDING}px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  box-sizing: border-box;

  @media screen and (min-width: 992px) {
    width: 100%;
    height: auto;
  }
`

export const ArticleWrap = styled.div`
  position: relative;
  ${(props) => (props.isBookmark ? 'height: 60%;' : '')}
  ${(props) => (props.isBookmark ? 'border-left: 1px solid rgba(38, 34, 34, 0.1);' : '')}
  ${(props) => (!props.isBookmark ? 'border-bottom: 1px solid rgba(38, 34, 34, 0.1);' : '')}
  ${(props) => (props.isBookmark ? `padding-left: ${GRID_PADDING}px;` : '')}

  @media screen and (min-width: 992px) {
    height: 100%;
    border-bottom: 1px solid rgba(38, 34, 34, 0.1);
    padding-left: 0;
    border-left: 0;
  }
`
