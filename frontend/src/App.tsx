import React from 'react'
import { Provider } from 'react-redux'
import Store from './Redux/Store'
import { Container, Col, Row } from 'react-grid-system'
import Header from './Components/Header'
import InitializeApp from './Utils/Initialize'
import CSSReset from './StyledComponents/CSSReset'
import ArticleList from './Components/ArticleList'
import BookmarkedList from './Components/BookmarkedList'

const App = () => {
  return (
    <Provider store={Store}>
      <InitializeApp>
        <>
          <CSSReset />
          <Header />
          <Container>
            <Row>
              <Col sm={12} lg={2} push={{ lg: 10 }}>
                <BookmarkedList />
              </Col>
              <Col sm={12} lg={10} pull={{ lg: 2 }}>
                <ArticleList />
              </Col>
            </Row>
          </Container>
        </>
      </InitializeApp>
    </Provider>
  )
}

export default App
