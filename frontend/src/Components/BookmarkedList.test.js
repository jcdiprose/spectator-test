import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import BookmarkedList from './BookmarkedList'
import { initial_state } from '../Redux/News/Reducer'
import { removeFromBookmarks } from '../Redux/News/Actions'
import MockData from '../TestData'

const data = MockData.map((article, index) => ({ ...article, id: index }))

const mockStore = configureStore()

test('Renders the BookmarkedList', () => {
  const store = mockStore({
    news: {
      ...initial_state,
      articles: data,
      bookmarked: [data[0], data[1]],
    },
  })

  const { getByText } = render(
    <Provider store={store}>
      <BookmarkedList />
    </Provider>
  )

  expect(getByText('Eve Sneider')).toBeInTheDocument()
})

test('Should call removeFromBookmarks with the article id', async () => {
  const store = mockStore({
    news: {
      ...initial_state,
      articles: data,
      bookmarked: [data[0], data[1]],
    },
  })

  store.dispatch = jest.fn()

  const { getAllByTestId } = render(
    <Provider store={store}>
      <BookmarkedList />
    </Provider>
  )

  const close_button = getAllByTestId('close-btn')[0]

  fireEvent.click(close_button)

  expect(store.dispatch).toHaveBeenCalledWith(removeFromBookmarks(0))
})
