import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import ArticleList from './ArticleList'
import { initial_state } from '../Redux/News/Reducer'
import { addToBookmarks } from '../Redux/News/Actions'
import MockData from '../TestData'

const data = MockData.map((article, index) => ({ ...article, id: index }))

const mockStore = configureStore()

test('Renders the ArticleList', () => {
  const store = mockStore({
    news: {
      ...initial_state,
      articles: data,
    },
  })

  const { getByText } = render(
    <Provider store={store}>
      <ArticleList />
    </Provider>
  )

  expect(getByText('Eve Sneider')).toBeInTheDocument()
})

test('Should call addToBookmarks with the article id', async () => {
  const store = mockStore({
    news: {
      ...initial_state,
      articles: data,
    },
  })

  store.dispatch = jest.fn()

  const { getAllByText } = render(
    <Provider store={store}>
      <ArticleList />
    </Provider>
  )

  const first_button = getAllByText('Add')[0]

  fireEvent.click(first_button)

  expect(store.dispatch).toHaveBeenCalledWith(addToBookmarks(0))
})
