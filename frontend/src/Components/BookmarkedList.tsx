import React from 'react'
import { Container, Col, Row } from 'react-grid-system'

import { useSelector } from 'react-redux'
import { Store } from '../Redux/Store'

import styled from 'styled-components'
import Article from './Article'
import { RegularGoudy, SansDisplay, Scroller, ScrollContainer, ScrollItem } from '../StyledComponents'
import { ArticleType } from '../GlobalTypes'
import { SCROLL_ITEM_WIDTH } from '../Constants/index.js'

export default () => {
  const bookmarked_articles = useSelector((store: Store) => store.news.bookmarked)
  const bookmark_title_width = 175

  const scrollable_width = bookmarked_articles.length * SCROLL_ITEM_WIDTH + bookmark_title_width

  return (
    <>
      {/* @ts-ignore */}
      <Scroller>
        {/* @ts-ignore */}
        <ScrollContainer scrollableWidth={scrollable_width} setHeight="200px">
          {/* @ts-ignore */}
          <ScrollItem setWidth={bookmark_title_width + 'px'}>
            {/* @ts-ignore */}
            <RegularGoudy setWidth="100px">Your bookmarks</RegularGoudy>
            <SansDisplay>Articles you bookmark will be added to this list</SansDisplay>
          </ScrollItem>
          {bookmarked_articles.map((article: ArticleType) => {
            return (
              <React.Fragment key={`bookmark-${article.id}`}>
                {/* @ts-ignore */}
                <ScrollItem>
                  <Article {...article} isBookmark />
                </ScrollItem>
              </React.Fragment>
            )
          })}
        </ScrollContainer>
      </Scroller>
    </>
  )
}
