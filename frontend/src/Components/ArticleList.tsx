import React from 'react'
import { Col, Row } from 'react-grid-system'

import { useSelector } from 'react-redux'
import { Store } from '../Redux/Store'

import Article from './Article'
import { ArticleType } from '../GlobalTypes'

export default () => {
  const articles = useSelector((store: Store) => store.news.articles)

  return (
    <Row>
      {articles.map((article: ArticleType, index: number) => {
        return (
          <Col sm={12} md={4} key={article.id}>
            <Article {...article} />
          </Col>
        )
      })}
    </Row>
  )
}
