import React from 'react'
import { Author, Headline, Button, Image } from '../StyledComponents'

import { addToBookmarks, removeFromBookmarks } from '../Redux/News/Actions'

import { ArticleType } from '../GlobalTypes'
import { useDispatch, useSelector } from 'react-redux'
import { Store } from '../Redux/Store'
import styled from 'styled-components'

import { GRID_PADDING } from '../Constants'

import HeartIcon from '../Assets/Heart'
import TickIcon from '../Assets/Tick'
import CloseIcon from '../Assets/Close'
import { ArticleWrap } from '../StyledComponents'

const CloseWrapper = styled.div`
  position: absolute;
  top: ${GRID_PADDING}px;
  right: ${GRID_PADDING}px;
  color: #d30d1d;
  font-size: 20px;
  cursor: pointer;
`

interface ArticleProps {
  isBookmark?: boolean
}

export default (props: ArticleType & ArticleProps) => {
  const is_bookmarked = useSelector(
    (store: Store) => !!store.news.bookmarked.find((article: ArticleType) => article.id === props.id)
  )

  const dispatch = useDispatch()

  const toggleAddToBookmarks = () => {
    dispatch(addToBookmarks(props.id))
  }

  const toggleRemoveFromBookmarks = () => {
    dispatch(removeFromBookmarks(props.id))
  }

  return (
    <>
      {/* @ts-ignore */}
      <ArticleWrap isBookmark={props.isBookmark}>
        <Author>{props.author}</Author>
        {/* @ts-ignore */}
        <Headline isBookmark={props.isBookmark}>{props.title}</Headline>
        {props.isBookmark ? (
          <CloseWrapper>
            <CloseIcon onClick={toggleRemoveFromBookmarks} />
          </CloseWrapper>
        ) : (
          <>
            {/* @ts-ignore ts and styled-components not playing together and I don't know the solution */}
            <Button is_bookmarked={is_bookmarked} onClick={toggleAddToBookmarks}>
              {is_bookmarked ? <span>Added</span> : <span>Add</span>}
              &nbsp;
              {is_bookmarked ? <TickIcon /> : <HeartIcon />}
            </Button>
          </>
        )}
        {!props.isBookmark && <Image src={props.urlToImage} alt={props.description} />}
      </ArticleWrap>
    </>
  )
}
