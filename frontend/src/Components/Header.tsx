import React from 'react'
import styled from 'styled-components'
import Logo from '../Assets/Logo'

const StyledHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: center;
  color: #d30d1d;
  padding: 0 10%;
  height: 200px;

  svg {
    width: 100%;
    display: block;
    max-width: 350px;
    height: auto;
  }
`

const BottomLine = styled.div`
  background: #d30d1d;
  height: 3px;
  width: calc(100% - 16px);
  margin-left: 16px;

  &::after {
    position: absolute;
    display: block;
    content: '';
    width: 100%;
    background: #d30d1d;
    height: 1px;
    box-shadow: 0px 8px 0px 0px rgba(211, 13, 29, 0.2), 0px 14px 0px 0px rgba(211, 13, 29, 0.2);
  }

  @media screen and (min-width: 768px) {
    width: 100%;
    background: #ebebeb;
    height: 1px;
    margin-left: 0px;

    &::after {
      height: 0;
      box-shadow: none;
      background: transparent;
    }
  }
`

export default () => {
  return (
    <>
      <StyledHeader>
        <Logo />
      </StyledHeader>
      <BottomLine />
    </>
  )
}
