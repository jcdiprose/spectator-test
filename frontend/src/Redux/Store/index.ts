import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import newsReducer from '../News/Reducer'
import { InitialState as NewsState } from '../News/NewsTypes'

const combined = combineReducers({
  news: newsReducer,
})

export interface Store {
  news: NewsState
}

/*eslint-disable */
const middleware: any[] = [thunk]
/*eslint-enable */

if (process.env.NODE_ENV === 'development') middleware.push(logger)

export default createStore(combined, applyMiddleware(...middleware))
