import reducer, { initial_state } from './Reducer'
import * as Types from './ActionTypes'

describe('News reducer', () => {
  it(`Should handle ${Types.NEWS_FETCHING}`, () => {
    expect(
      reducer(initial_state, {
        type: Types.NEWS_FETCHING,
      })
    ).toEqual({
      ...initial_state,
      loading: true,
    })
  })

  it(`Should handle ${Types.NEWS_SUCCESSFUL_FETCH}`, () => {
    expect(
      reducer(initial_state, {
        type: Types.NEWS_SUCCESSFUL_FETCH,
        payload: {
          data: [{ name: '123' }, { name: '456' }],
        },
      })
    ).toEqual({
      loading: false,
      bookmarked: [],
      fetch_error: null,
      articles: [
        { name: '123', id: expect.any(String) },
        { name: '456', id: expect.any(String) },
      ],
    })
  })

  it(`Should handle ${Types.ADD_TO_BOOKMARKS}`, () => {
    const articles = [
      { name: '123', id: 'id-123' },
      { name: '456', id: 'id-456' },
    ]

    expect(
      reducer(
        {
          ...initial_state,
          articles,
        },
        {
          type: Types.ADD_TO_BOOKMARKS,
          payload: 'id-456',
        }
      )
    ).toEqual({
      ...initial_state,
      articles,
      bookmarked: [{ name: '456', id: 'id-456' }],
    })
  })

  it(`Should handle ${Types.REMOVE_FROM_BOOKMARKS}`, () => {
    const articles = [
      { name: '123', id: 'id-123' },
      { name: '456', id: 'id-456' },
    ]

    expect(
      reducer(
        {
          ...initial_state,
          articles,
          bookmarked: [{ name: '456', id: 'id-456' }],
        },
        {
          type: Types.REMOVE_FROM_BOOKMARKS,
          payload: 'id-456',
        }
      )
    ).toEqual({
      ...initial_state,
      articles,
      bookmarked: [],
    })
  })

  it(`Should handle ${Types.NEWS_FAILED_FETCH}`, () => {
    try {
      const Error = new Error()
    } catch (error) {
      expect(
        reducer(
          {
            ...initial_state,
          },
          {
            type: Types.NEWS_FAILED_FETCH,
            payload: error,
          }
        )
      ).toEqual({
        ...initial_state,
        fetch_error: error,
      })
    }
  })
})
