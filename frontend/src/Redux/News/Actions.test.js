import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from './Actions'
import { initial_state } from '../../Redux/News/Reducer'
import * as Types from './ActionTypes'

import MockData from '../../TestData'

import { getNews } from '../../Api'
jest.mock('../../Api')

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Characters actions', () => {
  it('Fetches some news articles', () => {
    getNews.mockImplementation(() => new Promise((resolve, reject) => resolve(MockData)))

    const expectedActions = [{ type: Types.NEWS_FETCHING }, { type: Types.NEWS_SUCCESSFUL_FETCH, payload: MockData }]
    const store = mockStore({
      ...initial_state,
    })

    return store.dispatch(actions.requestNews()).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('Fails gracefully when fetching news articles', () => {
    getNews.mockImplementation(() => new Promise((resolve, reject) => reject('error')))

    const expectedActions = [
      { type: Types.NEWS_FETCHING },
      { type: Types.NEWS_FAILED_FETCH, payload: expect.anything() },
    ]

    const store = mockStore({
      ...initial_state,
    })

    return store.dispatch(actions.requestNews()).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('Adds a bookmark', () => {
    const expectedAction = { type: Types.ADD_TO_BOOKMARKS, payload: 'id-123' }
    expect(actions.addToBookmarks('id-123')).toEqual(expectedAction)
  })

  it('Removes a bookmark', () => {
    const expectedAction = { type: Types.REMOVE_FROM_BOOKMARKS, payload: 'id-123' }
    expect(actions.removeFromBookmarks('id-123')).toEqual(expectedAction)
  })
})
