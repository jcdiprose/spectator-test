import { Dispatch } from 'redux'
import {
  NEWS_FETCHING,
  NEWS_SUCCESSFUL_FETCH,
  NEWS_FAILED_FETCH,
  ADD_TO_BOOKMARKS,
  REMOVE_FROM_BOOKMARKS,
} from './ActionTypes'
import { getNews } from '../../Api'

export const requestNews = () => async (dispatch: Dispatch) => {
  dispatch({
    type: NEWS_FETCHING,
  })

  try {
    const articles = await getNews()
    dispatch({
      type: NEWS_SUCCESSFUL_FETCH,
      payload: articles,
    })
  } catch (error) {
    dispatch({
      type: NEWS_FAILED_FETCH,
      payload: error,
    })
  }
}

export const addToBookmarks = (id: string) => ({
  type: ADD_TO_BOOKMARKS,
  payload: id,
})

export const removeFromBookmarks = (id: string) => ({
  type: REMOVE_FROM_BOOKMARKS,
  payload: id,
})
