import produce, { Draft, enableES5 } from 'immer'
import {
  NEWS_FETCHING,
  NEWS_SUCCESSFUL_FETCH,
  NEWS_FAILED_FETCH,
  ADD_TO_BOOKMARKS,
  REMOVE_FROM_BOOKMARKS,
} from './ActionTypes'
import { InitialState, NewsAction } from './NewsTypes'
import { ArticleType } from '../../GlobalTypes'
import uniqid from 'uniqid'

enableES5()

export const initial_state: InitialState = {
  loading: true,
  articles: [],
  bookmarked: [],
  fetch_error: null,
}

export default produce((draft: Draft<InitialState>, action: NewsAction) => {
  switch (action.type) {
    case NEWS_FETCHING:
      draft.loading = true
      break

    case NEWS_SUCCESSFUL_FETCH:
      draft.loading = false
      draft.articles = action.payload.data.map((article: ArticleType) => ({ ...article, id: uniqid() }))

      break
    case NEWS_FAILED_FETCH:
      draft.fetch_error = action.payload
      window.alert(action.payload.request.statusText)

      break

    case ADD_TO_BOOKMARKS:
      const article = draft.articles.find((article: ArticleType) => article.id === action.payload)
      if (article) draft.bookmarked.push(article)

      break

    case REMOVE_FROM_BOOKMARKS:
      draft.bookmarked = draft.bookmarked.filter((article: ArticleType) => article.id !== action.payload)
      break
  }
}, initial_state)
