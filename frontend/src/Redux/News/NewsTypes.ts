import { AxiosError, AxiosResponse } from 'axios'
import {
  ADD_TO_BOOKMARKS,
  NEWS_FAILED_FETCH,
  NEWS_FETCHING,
  NEWS_SUCCESSFUL_FETCH,
  REMOVE_FROM_BOOKMARKS,
} from './ActionTypes'

import { ArticleType } from '../../GlobalTypes'

export interface InitialState {
  loading: boolean
  articles: ArticleType[]
  fetch_error: null | AxiosError
  bookmarked: ArticleType[]
}

interface NewsLoading {
  type: typeof NEWS_FETCHING
}

interface NewsSuccess {
  type: typeof NEWS_SUCCESSFUL_FETCH
  payload: AxiosResponse<ArticleType[]>
}

interface NewsFailed {
  type: typeof NEWS_FAILED_FETCH
  payload: AxiosError
}

interface AddToBookmarks {
  type: typeof ADD_TO_BOOKMARKS
  payload: string
}

interface RemoveFromBookmarks {
  type: typeof REMOVE_FROM_BOOKMARKS
  payload: string
}

export type NewsAction = NewsLoading | NewsSuccess | NewsFailed | AddToBookmarks | RemoveFromBookmarks
