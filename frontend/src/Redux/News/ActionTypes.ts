export const NEWS_FETCHING = 'news/loading'
export const NEWS_SUCCESSFUL_FETCH = 'news/success'
export const NEWS_FAILED_FETCH = 'news/error'

export const ADD_TO_BOOKMARKS = 'news/add-to-bookmarks'
export const REMOVE_FROM_BOOKMARKS = 'news/remove-from-bookmarks'
