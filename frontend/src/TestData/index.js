export default [
  {
    source: { id: 'wired', name: 'Wired' },
    author: 'Eve Sneider',
    title: 'More Covid Vaccines in Phase III, New Risk Calculators, and More',
    description: 'Catch up on the most important coronavirus updates from this week.',
    url: 'https://www.wired.com/story/more-covid-vaccines-in-phase-iii-new-risk-calculators-and-more/',
    urlToImage:
      'https://media.wired.com/photos/5f6dfd3ed2d484b0a9fd894f/191:100/w_1280,c_limit/Science_newsletter_1221866390.jpg',
    publishedAt: '2020-09-25T18:22:56Z',
    content:
      'More vaccines enter Phase III trials, researchers continue to learn about the long-term impacts of Covid-19, and risk calculation becomes increasingly difficult as the country reopens. Heres your wee… [+3758 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Jonathan Wolfe',
    title: 'Coronavirus Briefing: What Happened Today',
    description: 'Experts are urging drug companies to be more transparent about how they’re testing vaccines.',
    url: 'https://www.nytimes.com/2020/09/14/us/coronavirus-today.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-09-14T22:13:52Z',
    content:
      'A growing number of independent scientists and public health officials are urging drug companies to be more transparent about how theyre running the clinical trials for their vaccines.\r\nTypically, dr… [+1223 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Jonathan Wolfe and Lara Takenaga',
    title: 'Coronavirus Briefing: What Happened Today',
    description: 'Nearly 200,000 people in the United States have died from the coronavirus.',
    url: 'https://www.nytimes.com/2020/09/21/us/coronavirus-today.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-09-21T21:52:32Z',
    content:
      'Nightlife and group activities, which resumed more quickly than in neighboring countries, have been blamed in part for Spains second wave. A rise in large family gatherings, the return of tourism and… [+1861 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Lara Takenaga and Jonathan Wolfe',
    title: 'Coronavirus Briefing: What Lies Ahead',
    description: 'Will the virus resurge with a vengeance?',
    url: 'https://www.nytimes.com/2020/09/03/us/coronavirus-briefing-what-lies-ahead.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-09-03T22:04:50Z',
    content:
      'Vaccine experts I talk to are very nervous right now.\r\nActually, many of them believe that by late December or January, we may have solid proof that one or even several vaccines are safe and effectiv… [+5144 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Jonathan Wolfe and Lara Takenaga',
    title: 'Coronavirus Briefing: What Happened Today',
    description: 'Campus outbreaks are beginning to infect surrounding communities.',
    url: 'https://www.nytimes.com/2020/08/31/us/coronavirus-today.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-08-31T21:50:36Z',
    content:
      'Wealth and privilege have always opened doors, and during the early days of the pandemic, they yielded access to coronavirus tests in short supply and a way to escape hard-hit areas.\r\nNow, in New Yor… [+5706 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Lara Takenaga and Jonathan Wolfe',
    title: 'Coronavirus Briefing: What Happened Today',
    description:
      'Who should get vaccines first? Bill Gates says deaths would be halved if rich countries didn’t get first dibs.',
    url: 'https://www.nytimes.com/2020/09/15/us/coronavirus-today.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-09-15T22:03:11Z',
    content:
      'With infections on the rise, European countries are taking a different approach to the virus as they face a possible second wave this fall. \r\nTheyre trying to avoid the harsh lockdown measures they e… [+1108 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Lara Takenaga and Jonathan Wolfe',
    title: 'Coronavirus Briefing: What Happened Today',
    description: 'The pandemic is making the record-setting fires in the West even harder to fight.',
    url: 'https://www.nytimes.com/2020/09/18/us/coronavirus-today.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-09-18T21:24:47Z',
    content:
      'In the U.S., in the midst of a pandemic, wildfires, and racial and political conflict, some Jews are struggling to identify with one of the themes of the season: renewal.\r\nI just have this incredible… [+1214 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Lara Takenaga and Jonathan Wolfe',
    title: 'Coronavirus Briefing: What Happened Today',
    description: 'Some researchers have begun testing experimental vaccines on themselves.',
    url: 'https://www.nytimes.com/2020/09/01/us/coronavirus-today.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-09-01T21:45:03Z',
    content:
      'Typically, playing at the U.S. Open, even for those who dont make it very far, comes with the perk of being able to enjoy New York Citys nightlife, neighborhoods or shopping during downtime. But not … [+4737 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Jonathan Wolfe',
    title: 'Coronavirus Briefing: What Happened Today',
    description: 'Research shows that coronavirus infections “are moving up the age bands”',
    url: 'https://www.nytimes.com/2020/09/24/us/coronavirus-today.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-09-24T22:30:41Z',
    content:
      'Young adults make up a growing percentage of coronavirus cases in the United States and Europe, and early indications are that they tend to fare better with the disease, suffering fewer deaths and ho… [+1109 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Lara Takenaga',
    title: 'Coronavirus Briefing: What Happened Today',
    description: 'Drugmakers pledged to not distribute a vaccine until it had been thoroughly tested.',
    url: 'https://www.nytimes.com/2020/09/08/us/coronavirus-briefing-what-happened-today.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/09/08/us/oakImage-1599590036498/oakImage-1599590036498-facebookJumbo.png',
    publishedAt: '2020-09-08T23:08:38Z',
    content:
      'Nine drug companies pledged today to stand with science and not release a coronavirus vaccine until it met rigorous safety and efficacy standards. Normally in competition with one another, the compan… [+1146 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Lara Takenaga',
    title: 'Coronavirus Briefing: What Happened Today',
    description: 'Architects think outside the six-foot bubble.',
    url: 'https://www.nytimes.com/2020/09/11/us/coronavirus-briefing-what-happened-today.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-09-11T21:14:29Z',
    content:
      'Crises, both natural and man-made, have long driven change in architecture and urban planning. In the coronavirus era, when indoor spaces have become danger zones, architects have been tasked with fi… [+944 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Jonathan Wolfe',
    title: 'Coronavirus Briefing: What Happened Today',
    description: 'Two drug companies released comprehensive road maps of how they are evaluating their vaccines.',
    url: 'https://www.nytimes.com/2020/09/17/us/coronavirus-today.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-09-17T23:43:47Z',
    content:
      'To prevent large family gatherings that could become petri dishes for the virus, people will be required to stay within about 600 yards of their homes. But there are lots of exceptions: work, exercis… [+867 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Jonathan Wolfe',
    title: 'Coronavirus Briefing: What Happened Today',
    description: 'A massive market in Mexico City is emblematic of the region’s struggles.',
    url: 'https://www.nytimes.com/2020/09/23/us/coronavirus-today.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-09-23T21:51:58Z',
    content:
      'Johnson &amp; Johnson has begun late-stage clinical trials for its coronavirus vaccine injecting a boost of optimism into the race to rein in Covid-19.\r\nWhile Johnson &amp; Johnson is the fourth comp… [+1614 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Jonathan Wolfe',
    title: 'Coronavirus Briefing: What Happened Today',
    description:
      'How close is herd immunity? Despite optimistic claims, up to 90 percent of the U.S. population is still vulnerable.',
    url: 'https://www.nytimes.com/2020/09/29/us/coronavirus-today.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-09-29T22:07:23Z',
    content:
      'After months of persistently driving down its virus numbers, New York City is witnessing a jump in virus cases a troubling sign as the city takes some major steps toward resuming public life.\r\nThe ci… [+1547 chars]',
  },
  {
    source: { id: 'wired', name: 'Wired' },
    author: 'Yi-Ling Liu',
    title: 'Chinese Users Turned GitHub into a Land of Free Covid Speech',
    description:
      'As China cracked down on the spread of coronavirus information behind its Great Firewall, GitHub became a refuge from censorship. It may not last long.',
    url: 'https://www.wired.com/story/china-github-free-speech-covid-information/',
    urlToImage:
      'https://media.wired.com/photos/5f4d7e243cf60fd0f7b476e9/191:100/w_1280,c_limit/github-censorship-china.jpg',
    publishedAt: '2020-09-09T11:00:00Z',
    content:
      'The platforms unique resilience can be explained through the Cute Cat Theory of Digital Activism, says Margaret Roberts, a professor studying Chinese censorship at UC San Diego. The theory, posited b… [+4751 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Lara Takenaga and Jonathan Wolfe',
    title: 'Coronavirus Briefing: A Summer of Lost Opportunity',
    description: 'The U.S. failed to stamp out the virus before the dangerous autumn and winter months.',
    url: 'https://www.nytimes.com/2020/09/04/us/coronavirus-briefing-a-summer-of-lost-opportunity.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/03/03/world/coronavirus-map-promo/coronavirus-map-promo-facebookJumbo-v1002.png',
    publishedAt: '2020-09-04T21:39:40Z',
    content:
      'As summer comes to a close, the United States is averaging about 40,000 new cases a day, down from a horrifying peak in late July. But in many ways, the country is worse off now than at the beginning… [+923 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Richard Pérez-Peña',
    title: 'Coronavirus Deaths Pass One Million Worldwide',
    description:
      'Over the past 10 months, the virus has taken more lives than H.I.V., malaria, influenza and cholera. And as it sows destruction in daily life around the globe, it is still growing quickly.',
    url: 'https://www.nytimes.com/2020/09/28/world/covid-1-million-deaths.html',
    urlToImage:
      'https://static01.nyt.com/images/2020/05/15/world/28virus-onemillion/merlin_172508022_69add9d4-cc46-4118-96a3-0f0c1654ec4e-facebookJumbo.jpg',
    publishedAt: '2020-09-29T00:42:32Z',
    content:
      'And, crucially, people are most contagious when they first show symptoms or even earlier, not days or weeks later, when they are sickest a reversal of the usual pattern with infectious diseases. That… [+1072 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Tara Parker-Pope',
    title: 'Does Wearing Glasses Protect You From Coronavirus?',
    description:
      'After researchers noticed fewer nearsighted patients in a hospital ward in China, they speculated that wearing glasses might offer some protection against Covid-19.',
    url: 'https://www.nytimes.com/2020/09/16/well/live/does-wearing-glasses-protect-you-from-coronavirus.html',
    urlToImage: 'https://static01.nyt.com/images/2020/09/16/well/well-glasses/well-glasses-facebookJumbo.jpg',
    publishedAt: '2020-09-16T22:04:22Z',
    content:
      'And Dr. Maragakis noted that any number of factors could confound the data, and it may be that wearing glasses is simply associated with another variable that affects risk for Covid-19. For example, … [+3223 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Valeriya Safronova',
    title: 'Capri, Famous Vacation Spot, Touched by Coronavirus',
    description:
      'Visitors from the United States make up the largest share of foreign tourists on the Italian island, and this year their absence is denting local businesses.',
    url: 'https://www.nytimes.com/2020/09/08/style/capri-a-getaway-for-the-rich-and-famous-misses-its-americans.html',
    urlToImage: 'https://static01.nyt.com/images/2020/09/01/fashion/VIRUS-CAPRI01/VIRUS-CAPRI01-facebookJumbo.jpg',
    publishedAt: '2020-09-08T14:03:25Z',
    content:
      'Michela De Martino, 44, who with her sister and uncle owns Da Paolino, a former farm and bocce court that became a restaurant in the 1970s, said that about 80 percent of their customers are from the … [+4670 chars]',
  },
  {
    source: { id: null, name: 'New York Times' },
    author: 'Valeriya Safronova',
    title: 'Capri, Famous Vacation Spot, Touched by Coronavirus',
    description:
      'Visitors from the United States make up the largest share of foreign tourists on the Italian island, and this year their absence is denting local businesses.',
    url: 'https://www.nytimes.com/2020/09/08/style/capri-coronavirus.html',
    urlToImage: 'https://static01.nyt.com/images/2020/09/01/fashion/VIRUS-CAPRI01/VIRUS-CAPRI01-facebookJumbo.jpg',
    publishedAt: '2020-09-08T14:03:25Z',
    content:
      'Michela De Martino, 44, who with her sister and uncle owns Da Paolino, a former farm and bocce court that became a restaurant in the 1970s, said that about 80 percent of their customers are from the … [+1464 chars]',
  },
]
