const axios = require("axios");

class News {
  constructor() {
    this.api_key = process.env.NEWSAPI_KEY;
  }

  coronavirus = (req, res) => {
    axios.defaults.headers.get["X-Api-Key"] = this.api_key;

    axios
      .get(`https://newsapi.org/v2/everything?q=coronavirus`)
      .then((result) => {
        res.status(200);
        res.send(result.data.articles);
      })
      .catch((err) => {
        res.status(err.response.status);
        res.send(err);
      });
  };
}

module.exports = new News();
