const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
dotenv.config();

const News = require("./News");

const app = express();

app.use(
  cors({
    origin: ["http://localhost:3000", "http://localhost:5000"],
  })
);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get("/coronavirus-news", News.coronavirus);

const PORT = 4000;
app.listen(PORT, () => console.log(`http://localhost:${PORT}`));
