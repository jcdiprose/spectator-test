This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

`npm i` in /backend

create .env in /backend with following

```
NEWSAPI_KEY = "***************"
```

`npm run start` in /backend

`npm i` in /frontend

`npm run build` in /frontend

`serve -s build` in /frontend
